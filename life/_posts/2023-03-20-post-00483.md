---
layout: post
title: "산업의 사이클과 주식시장에 대한 고찰- feat 한국 주식 장기투자하면..."
toc: true
---

 산업의 사이클과 주식시장에 대한 고찰- feat 한국 주식 장기투자하면 안되는 기쁨 대응법
 
미국 주식은 우상향하니 미국 주식에 투자해야 한다.
동학개미운동에 맞서 서학개미열풍이 불었습니다.

2023년 3월 28일 오늘
서학개미든 동학개미든 대부분의 투자자가 좋은 결과를 얻지 못한거 같습니다.
상당수의 투자자가 고점에 주식을 매수해서요.

손실이 커서 조금만 올라도 조금만 떨어져도 일희일비하는 것 같아요.

그래서 오늘은 주식시장의 특유성 새중간 하나에 대해서 이야기를 해볼까 해요.

미국시장에 대한 분석은 차후에 하구요.

왜 한국 주식시장은 박스권에서 움직이나?
우리나라 기업의 주가는 미국기업들처럼 우상향하지 못하나? 에 대해서 분석해볼까 해요.

1. 한국의 주력 산업은 뭘까요?
2. 한국은 어떻게 먹고 살아갈까요?

약간의 이견차이는 있겠지만요.
대부분의 한국인이라면 이전 질문에 대해서 꽤 명쾌한 답을 내놓을 꺼 같네요.

1번에 대한 답
반도체, 자동차, 조선등등
물론 시대에 그래서 주력 산업이 변해왔지만요.
지난 20년을 돌이켜본다면 큰 범주에서 반도체, 자동차, 조선이였지요.
지금은 2차전지가 새로운 주력 산업을 주목받고 있지만요.

2번에 대한 답
답은 제품을 만들어서 수출해서 먹고 삽니다.
1번에 나온 주력 공 뿐만 아니라 농축산물 그외 제약, 방산 등등 다른 수출품도 많기는 하지만요.
그렇게 벌어들인 수익으로 석유도 사고, 공산품 농축산물등등을 수입해서 살지요.

왜 이런즉 이야기를 하냐면 우리나라의 주식을 발행한 회사를 통틀어서 (상장 회사와 비상장회사 구분없이)주식회사들은 이런 수출품들을 만드는 회사가 대강령 입니다.

그런데 대부분의 수출품이 경제의 순환 사이클인 경기를 탑니다.
경제가 호황일때는 수출이 잘되고, 경제가 불황일 때는 제품이 수출이 잘되지 않겠죠?

이런 상황이라면 주식회사 중에서 주식을 상장한 상장회사들의 (기업의 가치를 반영한)주가 또한
그런 흐름을 반영하지 않을까요?

여기에 정답이 있습니다.

왜 우리나라 주식은 이런즉 경기사이클 상황을 자주 반영하다보니깐
주가가 반복적인 등락이 있습니다.
물론 장기적으로는 큰 폭은 아니지만 우상향하구요.

제가 예전에 [주식 시장](https://unwieldypocket.com/life/post-00041.html) 읽었던 리포트들입니다.
대부분이 당시의 가격보다 낮아졌거나 비슷합니다.

왜냐하면 경기에 민감한 산업들이 대부분이여서요.
우량주에 장기투자해도 비슷한 결과를 얻었을 껍니다.

예전에 홍사훈의 경제쇼 플러스에서 우량주 장기투자의 위험성에 대한 내용을 다룬걸
요약해서 올려놨었는데요.
2021.08.10 - [경제 연구팀/투자] - [홍사훈의 경제쇼 플러스] “주식은 위험해~ 공부해서 우량주 방찰 장기간 보유하다 폭망한 본보기 총집합” | KBS 210807 방송
 
어떻게 보면 우량주 장기투자의 위험성에 대한 이론적인 설명이라고 보시면 됩니다.

종목에 집중하기보다는 산업의 사이클에 대해서
그리고 국민경제학 전체의 사이클에 대해서 더욱 관심을 가지고 투자해야 한다고 생각합니다.

차트나 항간에 떠도는 정보가 아니라요.

그런건 어떻게 하냐구요?
경제의 흐름과 지표를 알려주는 책을 읽으면서 공부해는 걸 추천드려요.
아래 책은 그런 내용을 빈번히 담고 있어서 추천드려요!!

 
아래에 참고리포트들이 있으니깐
참고로 보시기 바랍니다.

사진으로 올려놔서 보기가 다소 불편하실수 있겠지만요.
사진 속의 리포트에 당시의 가격과 리포트 작성기관 현재가등이 몽땅 나와 있습니다.
장기투자를 했다면 대부분은 좋은 결과를 얻지 못했을 껍니다.
 
"이 포스팅은 쿠팡 파트너스 활동의 일환으로, 이에 따른 일정액의 수수료를 제공받습니다."
