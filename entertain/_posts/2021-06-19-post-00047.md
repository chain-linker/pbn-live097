---
layout: post
title: "넷플릭스 추천 드라마"
toc: true
---

 넷플릭스 추천 드라마. 오늘은 장근 한국 드라마만 가져와봤다. 내가 스스로 보고 재밌었다고 느낀 작품들만 가져왔으니 믿어봐도 좋다. 오늘도 과연 분위기에 따라 취향에 따라 분류해서 추천해보겠다.
 
 

 1. <장르물 + 약간의 로맨스> 빈센조, 시그널

 정작 이 두 드라마는 분위기가 다른 거 같지만 아울러 넣어봤다. 비슷한 장르는 아니지만 연애 감정선이 약한 편이고, 세상과 맞서 싸우는 인물들이 등장하는 현실적인 드라마라고 생각했기 때문이다. 세상을 현실적으로 그려냈다는 뜻이고 마피아가 등장하는 빈센조나, 과거와 미래가 연결되는 무전기가 나오는 시그널이 현실적이란 뜻은 아니다.
 '빈센조'는 올해 했던 드라마라 적잖이 요사이 드라마라고 할 수명 있다. 빈센조는 포스터도 그렇고 송중기가 수트 입고 와인잔 들고 있는 구성 보면 영화 '위대한 개츠비'가 연상되는데, 실은 수모 관련이 없다. 빈센조는 이태리 마피아이며 변호사로 한국에 금을 찾으러 들어오며 이야기가 전개된다. 빈센조는 기업, 검찰, 경찰, 정부까지 건드리는 거대한 이야기이지만 대체적으로 유쾌한 분위기로 진행된다. 잘 웃으며 볼 행운 있는 드라마이면서도 탄탄한 스토리, 명품 배우들의 연기, 거기에 송중기의 기가막힌 액션까지 볼 명맥 있다. 개인적으로는 한국에서 예 힘든 드라마라고 느꼈다. 극 막바지에 로맨스도 은근히 챙기긴 하니까 다양한 시청자를 만족시킬 운명 있는 드라마다. 송중기 팬이라면, 전여빈 팬이라면, 이태리 변호사가 궁금하다면, 영화같은 퀄리티의 드라마가 보고 싶다면 추천!
 '시그널'은 시즌 2를 간절하게 기다리고 있다. 김은희 작가님이 킹덤으로 푹 돼서 무지무지 좋지만 시그널2를 혹여나 쓰실 생각이 없으신 건 아닌지 궁금해하는 중이다. 무론 그런 재능을 가졌지만 몸이 한개이고 시간도 한정적인지라 많은 작품을 한번에 쓰지 못하는 게 개탄스럽다. 시그널은 시간대를 이어주는 무전기가 등장해서 판타지적 요소를 지니지만, 하도 현실적인 드라마라고 느꼈다. 아울러 납치와 같은 범죄를 다루다보니 밤길이 무서워지는 부작용도 있다. 뿐만 아니라 시즌1이 굉장한 궁금증을 남기고 끝냈기 때문에 시즌2를 의견 빠지게 기다리게 된다는 부작용도 있다. 역으로 그쯤 재밌단 뜻이기도 하다. 대체 추천. 이제훈, 김혜수, 조진웅이 등장하며 명품 연기, 탄탄한 스토리, 거기에 스릴 만점까지. 이런 장르를 좋아한다면 꼭 보는 걸 추천한다. 넷플릭스는 속히 시고로 드라마를 훨씬 데려오고 시그널 2도 데려와라.
 

 

 2. <웹툰 원작 드라마, 송강> 좋아하면 울리는, 알고 있지만, 미생

 미생을 어디로 넣을지 고민하다가 무심히 여기에 넣어봤다. '좋아하면 울리는'과 '알고 있지만'은 둘다 웹툰을 원작으로 하며 주연으로 송강이 출연했다. 미생은 웹툰을 원작으로 그럼에도 불구하고 송강은 출연하진 않고 다른 장르이지만 볼만한 드라마인 건 분명하다.
 '좋아하면 울리는'은 넷플릭스 오리지널 드라마로 동명의 웹툰을 원작으로 했다. 시즌 1은 송강이 남아 주인공 포지션이고, 시즌 2에서는 덤 남주 포지션으로 바뀌는데 이는 원작 웹툰의 스토리를 따른 것으로 보인다. 개인적으로는 시즌1은 짜릿하게 재밌고 시즌2는 볼 만하다. 시즌1의 짜릿함은 학원물이라 교복을 입고 풋풋하고 설레는 고등학생들의 연애를 보여준다는 것도 경계 몫 한다. 지금의 송강을 거대한 주연으로 자리매김 시켜준 드라마라고 봐도 무방하다. 학원물 좋아한다면 추천, 송강 팬이라면 추천, 무겁지 않은 로맨스를 좋아한다면 추천, 지하철용 킬링 타입 드라마를 찾고 있다면 추천한다.
 '알고 있지만'은 입때 복판 봤는데, 으레 볼 예정이라 넣어봤다. 또한 송강이 주연으로 출연하는데, 한소희와 나란히 출연한다. 이윤 드라마도 동명의 웹툰을 원작으로 하는데, 한소희가 웹툰을 재밌게 봤다고 말하고 연기도 하게 돼서 화제가 됐다. 나도 재밌게 본 웹툰인데 한소희 송강이 웹툰의 캐릭터와 싱크로율이 놀라울 정도로 높아서 기대중이다. 먼저 넷플릭스에 올라와 있으니 언제든 볼 수 있다. 어떤 주차에 한량 회분씩 올라와서 사변 여북 참다가 정주행 할 생각이다. 대학생의 박애 얘기를 가볍게 보고 싶다면 추천! 몰래 눈이 호강하는 드라마를 찾고 있어도 추천한다.
 '미생'은 무려 2014년도에 방영했던 드라마였는데 넷플릭스에서 발견해서 넣어뒀다. 임시완, 변요한을 지금의 위치에 있게 해준 드라마라고 생각한다. 강하늘의 깐깐한 특성 연기 참으로 볼 수 있고 사회초년생, 심지어 고졸출신 낙하산 계약직의 이야기라 꽤나 서럽고 공감되면서도 위로를 받을 운명 있는 드라마다. 참말 미생 인물들처럼 성심껏 살라고 권하고 싶진 않은데 로맨스가 지겹다면, 현실적인 얘기 좋아한다면, 바둑에 [인터넷 영화](https://leaktree.com/entertain/post-00004.html) 흥미가 있다면 추천한다.
 

 오늘은 끝내고 싶었는데 상금 추천 드라마가 많이 남아서 다음을 역 기약해본다. 이상으로 오늘의 넷플릭스 추천 드라마 글을 마친다.
 
## '세상만사 스토리' 카테고리의 다른 글
